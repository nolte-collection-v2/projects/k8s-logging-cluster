
#module "base" {
#  source            = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-col-basement.git//modules/wrapper"
#  k8s_subnet        = "10.10.21.0/24"
#  number_first_host = 131
#  number_of_hosts   = 139
#}
#

resource "kubernetes_namespace" "acc_namespace" {
  metadata {
    name = "logging"
  }
}

module "operator" {
  source = "/tf-k8s-logging/modules/operator"
}


module "install" {
  depends_on = [module.operator]
  source     = "/tf-k8s-logging/modules/install"
  namespace  = kubernetes_namespace.acc_namespace.metadata[0].name
}
