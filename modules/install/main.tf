
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = ""
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = ""
  
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}